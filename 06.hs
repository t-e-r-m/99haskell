isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome xs = ispalindrome [] xs xs
    where ispalindrome rev (x:xs) (_:_:ys) = ispalindrome (x:rev) xs ys
          ispalindrome rev (x:xs) [_]      = rev == xs
          ispalindrome rev xs []           = rev == xs

isPalindrome' :: (Eq a) => [a] -> Bool
isPalindrome' []  = True
isPalindrome' [_] = True
isPalindrome' xs  = head xs == last xs && (isPalindrome' $init$tail xs)

