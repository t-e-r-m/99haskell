myButLast :: [a] -> a
myButLast [] = error "enter a non-empty list"
myButLast [x] = error "list should have more than one entry"
myButLast [x,_] = x
myButLast (_:xs) = myButLast xs 
