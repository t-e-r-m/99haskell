{-
elementAt :: [a] -> Int -> a
elementAt x n
    | length x < 1 = error "enter a larger list"
    | length x < n = error "enter a larger list"
    | otherwise    = drop (length x - n) . reverse . head x 
-}
elementAt :: [a] -> Int -> a
elementAt x  0     = head x
elementAt (x:_) 1  = x
elementAt (_:xs) n = elementAt xs (n-1)

--This does not work correctly on invalid indexes and infinite lists, e.g.:
--elementAt'' [1..] 0
--
--nah, it does.

elementAt' :: [a] -> Int -> a
elementAt' x 0 = head x
elementAt' x n = head $ drop (n-1) x
