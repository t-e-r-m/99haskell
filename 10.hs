-- Run-length encoding of a list. Use the result of problem 
-- P09 to implement the so-called run-length encoding data 
-- compression method. Consecutive duplicates of elements are 
-- encoded as lists (N E) where N is the number of duplicates 
-- of the element E.
pack :: (Eq a) => [a] -> [[a]]
pack [] = []
pack [x] = [[x]]
pack (x:xs) = if elem x (head (pack xs))
              then (x:(head (pack xs))):(tail (pack xs))
              else [x]:(pack xs)

encode :: pack -> [(Int, String)]

